export default function () {
  return {
    user: {
      id: '',
      fullname: '',
      email: '',
      tutorial: Boolean,
      country: '',
      country_id: ''
    },
    auth: false
  }
}
