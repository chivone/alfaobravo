import utils from '../../utils/splitName';

export function SET_USER_STATE(state, payload) {


    state.user = {
        id: payload.id,
        firstname: utils.splitFirstname(payload.fullname),
        lastname: utils.splitLastname(payload.fullname),
        email: payload.email,
        img: payload.image ? payload.image : null,
        tutorial: payload.tutorial ? payload.tutorial : true,
        country: { id: payload.country_id, name: payload.country }
    }
}

export function ENABLE_AUTH(state) {
    state.auth = true;
}

export function DISABLE_AUTH(state) {
    state.auth = false;
}

export function DESTROY(state) {
    state.user = { id: '', fullname: '', email: '', img: '' }
}
