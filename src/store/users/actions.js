export function addUser(context, payload) {
    context.commit('SET_USER_STATE', payload)
}

export function enableAuth (context) {
    context.commit('ENABLE_AUTH')
}

export function disableAuth (context) {
    context.commit('DISABLE_AUTH')
}

export function destroyUser(context) {
    context.commit('DESTROY')
}