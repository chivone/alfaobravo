export function loadCategories(context, payload) {
    context.commit('SET_CATEGORIES_STATE', payload)
}

export function setCategoryStatus(context) {
    context.commit('CATEGORY_STATUS')
}

export function changeStatusToTrue(context, payload) {
    context.commit('CHANGE_STATUS_TO_TRUE', payload)
}

export function destroyCategories(context) {
    context.commit('DESTROY')
}
