export function SET_CATEGORIES_STATE(state, payload) {
    payload.forEach(category => {
        let format = {
            id: category.id,
            name: category.name,
            locked: category.locked,
            totalWords: category.total_words,
            percentage: parseFloat(category.percentage.toFixed(2)),
            select: false,
        }

        state.categories.push(format)
    })
}

export function CATEGORY_STATUS(state) {
    state.categories.forEach(category => category.select = false)
}

export function CHANGE_STATUS_TO_TRUE(state, id) {
    let findCategory = state.categories.find(category => category.id == id)
    findCategory.select = true
}

export function DESTROY(state, category) {
    state.categories = []
}
