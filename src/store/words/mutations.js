export function SET_WORDS_STATE(state, payload) {
    payload.forEach(word => {
        state.words.push(word)
    });
}

export function SET_WORDSLIST_STATE(state, payload) {
    state.list = payload
}

export function ADD_WORD_TO_STATE(state, payload) {
    state.words.push(payload)
}

export function ADD_WORD_TO_LIST(state, payload) {
    state.list.push(payload)
}

export function SET_TOTAL_WORDS(state, payload) {
    state.totalWords = payload
}

export function SET_SCROLL_STATE(state, payload) {
    state.scroll = payload
}

export function SET_SCROLLED_STATE(state, payload) {
    state.scrolled = payload
}

export function INCREMENT_POSITION(state) {
    state.wordPosition++
}

export function RESET_WORDS(state) {
    state.words = []
    state.list = []
}

export function DESTROY(state) {
    state.words = []
    state.list = []
    state.scroll = 0
    state.scrolled = null
    state.wordPosition = 0
    state.totalWords = 0
}
