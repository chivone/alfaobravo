export default function () {
  return {
    words: [],
    list: [],
    totalWords: 0,
    scroll: 0,
    scrolled: null,
    wordPosition: 0
  }
}
