export function loadWords(context, payload) {
    context.commit('SET_WORDS_STATE', payload)
}

export function loadWordsList(context, payload) {
    context.commit('SET_WORDSLIST_STATE', payload)
}

export function loadWord(context, payload) {
    context.commit('ADD_WORD_TO_STATE', payload)
}

export function addWordList(context, payload) {
    context.commit('ADD_WORD_TO_LIST', payload)
}

export function setTotalWords(context, payload) {
    context.commit('SET_TOTAL_WORDS', payload)
}

export function setScroll(context, payload) {
    context.commit('SET_SCROLL_STATE', payload)
}

export function setScrolled(context, payload) {
    context.commit('SET_SCROLLED_STATE', payload)
}

export function incrementPosition(context) {
    context.commit('INCREMENT_POSITION')
}

export function resetWords(context) {
    context.commit('RESET_WORDS')
}

export function destroyWords(context) {
    context.commit('DESTROY')
}