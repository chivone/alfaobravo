export function SET_COUNTRIES_STATE(state, payload) {
    payload.forEach(country => {
        state.countries.push(country)
    });
}

export function DESTROY(state) {
    state.countries = []
}
