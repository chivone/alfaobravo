export function addCountries(context, payload) {
    context.commit('SET_COUNTRIES_STATE', payload)
}

export function destroyCountries(context) {
    context.commit('DESTROY')
}