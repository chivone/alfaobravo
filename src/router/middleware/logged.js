import { LocalStorage } from 'quasar'
import user from '../../helper/auth'
import { Loading } from 'quasar';

export default async function auth(to, from, next) {
  if (user.getUserLogged()) {
    try {
      Loading.show()
      let res = await user.show();
      if (LocalStorage.getItem('userInfo')) LocalStorage.remove('userInfo')

      let userData = {
        id: res.data.id,
        fullname: res.data.fullname,
        email: res.data.email,
        img: res.data.img,
        tutorial: res.data.confirm,
        country: res.data.country,
        country_id: res.data.country_id,
        password: res.data.password
      }
      LocalStorage.set('userInfo', userData)
      Loading.hide()
      return next({ name: 'home' });
    } catch (error) {
      Loading.hide()
      if (LocalStorage.getItem('userInfo')) LocalStorage.remove('userInfo')
      user.deleteUserLogged();
      return next()
    }
  }
}



