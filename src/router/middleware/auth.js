import { LocalStorage } from 'quasar';
import user from '../../helper/auth'

export default async function auth(to, from, next) {

  if (user.getUserLogged()) {
    try {
      let res = await user.show();
      if (LocalStorage.getItem('userInfo')) LocalStorage.remove('userInfo')

      let userData = {
        id: res.data.id,
        fullname: res.data.fullname,
        email: res.data.email,
        img: res.data.img,
        tutorial: res.data.confirm,
        country: res.data.country,
        country_id: res.data.country_id,
        password: res.data.password
      }

      LocalStorage.set('userInfo', userData)
    } catch (error) {
      if (LocalStorage.getItem('userInfo')) LocalStorage.remove('userInfo')
      user.deleteUserLogged();
      return next({ name: 'auth' });
    }
  } else {
    if (LocalStorage.getItem('userInfo')) LocalStorage.remove('userInfo')
    return next({ name: 'auth' });
  }
  return next();
}