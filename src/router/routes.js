import auth from './middleware/auth';
import logged from './middleware/logged';

const routes = [
  {
    path: '/',
    name: 'welcome',
    component: () => import('pages/Index.vue'),
    meta: {
      middleware: [logged]
    }
  },
  {
    path: '/auth', component: () => import('pages/PageAuth.vue'),
    name: 'auth',
    meta: {
      middleware: [logged],
    },
  },
  {
    path: '/terminos', component: () => import('pages/Terminos.vue'),
    name: 'terminos',
    meta: {
      middleware: [logged],
    },
  },
  {
    path: '/politica', component: () => import('pages/Politica.vue'),
    name: 'politica',
    meta: {
      middleware: [logged],
    },
  },
  {
    path: '/app',
    name: 'app',
    component: () => import('layouts/MainLayout.vue'),

    children: [
      {
        path: '', component: () => import('pages/PanelUsuario.vue'),
        name: 'home',
        meta: {
          middleware: [auth],
          pageTitle: 'Inicio',
        },
      },
      {
        path: 'exam/config/:categorySelectId', component: () => import('pages/HacerExamen.vue'),
        name: 'exam-config',
        meta: {
          middleware: [auth],
          pageTitle: 'Crear examen',
        },
      },
      {
        path: 'exam/:id', component: () => import('pages/Examen.vue'),
        name: 'exam',
        meta: {
          middleware: [auth],
          pageTitle: 'Examen',
        },
      },
      {
        path: 'exam/result/:id', component: () => import('pages/ResultadoExamen.vue'),
        name: 'exam-result',
        meta: {
          middleware: [auth],
          pageTitle: 'Resultado del examen',
        },
      },
      {
        path: 'category/show/:id', component: () => import('pages/Categoria.vue'),
        name: 'category-show',
        meta: {
          middleware: [auth],
          pageTitle: 'Categoria',
        },
      },
      {
        path: 'word/add/:categorySelectId', component: () => import('pages/AddWord.vue'),
        name: 'word-add',
        meta: {
          middleware: [auth],
          pageTitle: 'Agregar palabra',
        },
      },
      {
        path: 'word/list/:categoryId', component: () => import('pages/WordList.vue'),
        name: 'word-list',
        meta: {
          middleware: [auth],
          pageTitle: 'Listado de palabras',
        },
      },
      {
        path: 'word/:id?category_id=:categoryId', component: () => import('pages/Word.vue'),
        name: 'word',
        meta: {
          middleware: [auth],
          pageTitle: 'Palabras',
        },
      },
      {
        path: 'user/setting/', component: () => import('pages/Config.vue'),
        name: 'user-setting',
        meta: {
          middleware: [auth],
          pageTitle: 'Configuración',
        },
      },
      {
        path: 'user/edit/', component: () => import('components/UpdateUser.vue'),
        name: 'user-edit',
        meta: {
          middleware: [auth],
          pageTitle: 'Editar Perfil',
        }
      },
    ]
  },
  { path: '/tutorial', component: () => import('pages/Tutorial.vue') },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
