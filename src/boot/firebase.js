import firebase from "firebase/app";
import "firebase/auth"

const firebaseConfig = {
    apiKey: "AIzaSyDnEGfmkuOJAlAt4DrOnnq9xBV02_lrEoE",
    authDomain: "alfaobravo-oauth.firebaseapp.com",
    projectId: "alfaobravo-oauth",
    storageBucket: "alfaobravo-oauth.appspot.com",
    messagingSenderId: "164349345770",
    appId: "1:164349345770:web:cac085f4141e31c3c0fc93"
};

//Initialize Firebase
let firebaseAuth = firebase.initializeApp(firebaseConfig);

const provider = new firebase.auth.GoogleAuthProvider()

export { firebaseAuth, provider }