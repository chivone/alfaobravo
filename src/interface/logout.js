const logoutType = {
  ALL: 'all',
  CURRENT: 'current',
  OTHERS: 'others',
}

module.exports = {
    logoutType
};