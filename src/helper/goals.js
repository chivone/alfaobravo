import axios from "axios";
import auth from './auth';

const { api } = require('../config/apiConfig');

export default {
    index() {
        return axios.get(api.goals.index, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    show(id) {
        return axios.get(api.goals.show + '/show/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    store(goal) {
        return axios.post(api.goals.store, goal, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    update(goal) {
        return axios.put(api.goals.update, goal, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    delete(id) {
        return axios.delete(api.goals.destroy + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
};