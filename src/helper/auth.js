import axios from "axios";
import { LocalStorage } from "quasar";

const { api } = require("../config/apiConfig");
const { config } = require("../config/config");
const { logoutType } = require("../interface/logout");
import utils from "../utils/splitName";

export default {
  deleteUserLogged() {
    LocalStorage.remove("token");
  },
  setUserLogged(userLogged) {
    LocalStorage.set("token", userLogged);
  },
  getUserLogged() {
    return LocalStorage.getItem("token");
  },
  googleOauth(userData) {
    return axios.post(api.oauth.google_oauth, userData);
  },
  register(formData) {
    const user = {
      firstname: utils.formatFirstname(formData.firstname),
      lastname: utils.formatLastname(formData.lastname),
      email: formData.email,
      password: formData.password,
      password_confirmation: formData.password_confirmation,
      country_id: formData.country_id
    };
    return axios.post(api.oauth.user_store, user);
  },
  update(formData) {
    const user = {
      firstname: utils.formatFirstname(formData.firstname),
      lastname: utils.formatLastname(formData.lastname),
      email: formData.email,
      country_id: formData.country_id
    };
    return axios.put(api.oauth.edit, user, {
      headers: { Authorization: "Bearer " + this.getUserLogged() }
    });
  },
  passwordReset(formData) {
    const data = {
      password: formData.actual_password,
      new_password: formData.password,
      new_password_confirmation: formData.password_confirmation
    };

    return axios.put(api.oauth.password_reset, data, {
      headers: { Authorization: "Bearer " + this.getUserLogged() }
    });
  },
  login(formData) {
    return axios.post(api.oauth.login, {
      grant_type: "password",
      client_id: config.CLIENT_ID,
      client_secret: config.CLIENT_SECRET,
      password: formData.password,
      username: formData.email
    });
  },
  logout() {
    let token = this.getUserLogged();
    this.deleteUserLogged();
    return axios.get(api.oauth.logout + logoutType.CURRENT, {
      headers: { Authorization: "Bearer " + token }
    });
  },
  show() {
    return axios.get(api.oauth.user_show, {
      headers: { Authorization: "Bearer " + this.getUserLogged() }
    });
  },
  delete() {
    return axios.delete(api.oauth.user_delete, {
      headers: { Authorization: "Bearer " + this.getUserLogged() }
    });
  },
  resetProfile() {
    return axios.get(api.oauth.reset_profile, {
      headers: { Authorization: "Bearer " + this.getUserLogged() }
    });
  },
  resetProbability() {
    return axios.get(api.oauth.reset_probability, {
      headers: { Authorization: "Bearer " + this.getUserLogged() }
    });
  }
};
