import axios from "axios";
import auth from './auth';

const { api } = require('../config/apiConfig');

export default {
    wordsAll(word) {
        return axios.get(api.searcher.words_all, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
            params: { word: word.val }
        });
    },
    wordsCategoryAll(word, categoryId) {
        return axios.get(api.searcher.words_category_all, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
            params: { word: word, categoryId: categoryId }
        });
    },
    wordsCategoryCorrect(word, categoryId) {
        return axios.get(api.searcher.words_category_correct, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
            params: { word: word, categoryId: categoryId }
        });
    },
    wordsCategoryIncorrect(word, categoryId) {
        return axios.get(api.searcher.words_category_incorrect, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
            params: { word: word, categoryId: categoryId }
        });
    },
};