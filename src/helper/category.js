import axios from "axios";
import auth from './auth';

const { api } = require('../config/apiConfig');

export default {
    index() {
        return axios.get(api.categories.index, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    indexDefault() {
        return axios.get(api.categories.index_default, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    free() {
        return axios.get(api.categories.free, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    show(id) {
        return axios.get(api.categories.show + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    store(name) {
        const data = {
            name: name
        };
        return axios.post(api.categories.store, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    update(id, name) {
        const data = {
            name: name
        };
        return axios.put(api.categories.update + '/' + id, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    delete(id) {
        return axios.delete(api.categories.destroy + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
};