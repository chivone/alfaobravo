import axios from "axios";
import auth from './auth';

const { api } = require('../config/apiConfig');

export default {
    current() {
        return axios.get(api.exam.current, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    show(id) {
        return axios.get(api.exam.show + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    showResponde(id) {
        return axios.get(api.exam.show_response + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    store(amountWords, durationTime, categoryIdArray, ArrayLeves) {
        const data = {
            amountWords: amountWords,
            durationTime: durationTime,
            categoryIdArray: categoryIdArray,
            levelsArray: ArrayLeves
        };
        return axios.post(api.exam.store, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    storeResponse(wordsResponse, totalTime) {
        const data = {
            totalTimeDuration: totalTime,
            wordsArray: wordsResponse,
        };
        return axios.post(api.exam.store_response, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    delete(id) {
        return axios.delete(api.exam.destroy + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    reset(id) {
        return axios.get(api.exam.reset + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
};