import axios from "axios";
import auth from './auth';

const { api } = require('../config/apiConfig');

export default {
    index(categoryId) {
        return axios.get(api.words.index + categoryId, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    show(id, categoryId) {
        return axios.get(api.words.show + '/' + id,  {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
            params: { category_id: categoryId }
        });
    },
    NORAE() {
        return axios.get(api.words.norae, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    store(word, categoryId = null, wordIncorrectId = null, meaning = null ,nivel=null, anotacion=null, cursiva=null ) {
        const data = {
            word: word,
            word_incorrect_id: wordIncorrectId,
            meaning: meaning,
            nivel: nivel,
            anotacion: anotacion,
            cursiva: cursiva,
            category_id: categoryId,
        };
        return axios.post(api.words.store, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    update(id, word, meaning = null) {
        const data = {
            word: word,
            meaning: meaning,
        };
        return axios.put(api.words.update + '/' + id, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    delete(id) {
        return axios.delete(api.words.destroy + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
};