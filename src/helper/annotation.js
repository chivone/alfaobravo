import axios from "axios";
import auth from './auth';

const { api } = require('../config/apiConfig');

export default {
    index(id) {
        return axios.get(api.annotations.index + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    show(id) {
        return axios.get(api.annotations.show + '/show/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    store(word, text) {
        const data = {
            word_id: word,
            description: text,
        };
        return axios.post(api.annotations.store, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
    update(id, text) {
        const data = {
            id: id,
            description: text
        };
        return axios.put(api.annotations.update + '/' + id, data, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() },
        });
    },
    delete(id) {
        return axios.delete(api.annotations.destroy + '/' + id, {
            headers: { "Authorization": 'Bearer ' + auth.getUserLogged() }
        });
    },
};