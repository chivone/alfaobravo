/*
* CONFIG
* system configuration file,
* here you need to set environment variables.
* author: Gian Vespa
*/
module.exports.config = {

    /*
    * environments 
    * APP_ENV = 'local' || 'production'
    */
    APP_ENV: 'production',

    /*
    * backend api-res 
    * your url production the back-end
    */
    URL_BACK_PRODUCTION: 'https://alfaobravo.herokuapp.com/',

    /*
    * backend api-res 
    * your url local the back-end
    */
    URL_BACK_LOCAL: 'http://localhost/alfaobravo.backend/',

    /*
    * your port, defaul port 8080
    */
    PORT: 8080,

    /*
    * client
    * CLIENT_ID = added your id of client
    */
    CLIENT_ID: 2,

    /*
    * client secret
    * CLIENT_SECRET = added your token of client
    * Client secret the serve heroku = KxzUnrNGMC6FZcVFwOYqWpHMdZ1L5UKBNJ4wy5bL
    * Client secret serve local MySQL = vh2kg42iQ6UxXnJY0ZUHm1jN0DiSmrodiBlCw3Ge
    * Client secret serve local PostgreSQL = qsdGjAHAughpHsNXgxou091BPD3f95FpCHsLJAl4
    */
    CLIENT_SECRET: 'KxzUnrNGMC6FZcVFwOYqWpHMdZ1L5UKBNJ4wy5bL'
}