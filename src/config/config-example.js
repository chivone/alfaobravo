/*
* CONFIG
* system configuration file,
* here you need to set environment variables.
* author: Gian Vespa
*/
module.exports.config = {

    /*
    * environments 
    * APP_ENV = 'local' || 'production'
    */
    APP_ENV: 'local',

    /*
    * backend api-res 
    * your url production the back-end
    */
    URL_BACK_PRODUCTION: '',

    /*
    * backend api-res 
    * your url local the back-end
    */
    URL_BACK_LOCAL: 'http://localhost:8000/',

    /*
    * your port, defaul port 8080
    */
    PORT: 8080,

    /*
    * client
    * CLIENT_ID = added your id of client
    */
    CLIENT_ID: 3,

    /*
    * client secret
    * CLIENT_SECRET = added your token of client
    */
    CLIENT_SECRET: 'kSTYodDXXdJZ8HlvcNpFTTpLddpkVLkCVjN7J2nl'
}