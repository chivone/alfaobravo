const { serve } = require("./serveConfig");

/*
 * CONFIG API
 * Api configuration file,
 * here add the server paths.
 * author: Gian Vespa
 */

// variables
var api = "api/";
var categories = "categories";
var words = "words";
var annotations = "annotations";
var notifications = "notifications/";
var countries = "countries/";
var searcher = "searcher";
var exam = "exam";
var goals = "goals";

module.exports.api = {
  home: serve.url,
  // Api router user
  oauth: {
    user_store: serve.url + api + "user/register",
    user_show: serve.url + api + "user/show",
    admin_store: serve.url + api + "admin/register",
    admin_show: serve.url + api + "admin/show",
    login: serve.url + "oauth/token",
    logout: serve.url + api + "user/logout/",
    photo: serve.url + api + "user/photo",
    edit: serve.url + api + "user/edit",
    password_reset: serve.url + api + "user/password",
    user_delete: serve.url + api + "user/delete",
    reset_profile: serve.url + api + "user/reset/profile",
    reset_probability: serve.url + api + "user/reset/probability",
    google_oauth: serve.url + api + "google/auth"
  },

  // Api router searcher
  searcher: {
    words_all: serve.url + api + searcher + "/words/all",
    words_category_all: serve.url + api + searcher + "/words/category/all",
    words_category_correct:
      serve.url + api + searcher + "/words/category/correct",
    words_category_incorrect:
      serve.url + api + searcher + "/words/category/incorrect"
  },

  // Api router categories
  exam: {
    store: serve.url + api + exam,
    show: serve.url + api + exam, // {id}
    store_response: serve.url + api + exam + "/response",
    show_response: serve.url + api + exam + "/response/", // {id}
    current: serve.url + api + exam + "/current",
    destroy: serve.url + api + exam, // {id},
    reset: serve.url + api + exam + "/reset/"
  },

  // Api router categories
  categories: {
    index: serve.url + api + categories,
    index_default: serve.url + api + categories + "/default",
    free: serve.url + api + categories + "/free",
    store: serve.url + api + categories,
    show: serve.url + api + categories, // {id}
    update: serve.url + api + categories, // {id}
    destroy: serve.url + api + categories // {id}
  },

  // Api router words
  words: {
    index: serve.url + api + words + "/category/", // {categoryId}
    store: serve.url + api + words,
    norae: serve.url + api + words + "/NORAE",
    show: serve.url + api + words, // {id}
    update: serve.url + api + words, // {id}
    destroy: serve.url + api + words // {id}
  },

  // Api router annotations
  annotations: {
    index: serve.url + api + annotations,
    show: serve.url + api + annotations,
    store: serve.url + api + annotations,
    update: serve.url + api + annotations,
    destroy: serve.url + api + annotations
  },

  // Api router notifications
  notifications: {
    index: serve.url + api + notifications,
    show: serve.url + api + notifications,
    store: serve.url + api + notifications,
    update: serve.url + api + notifications,
    destroy: serve.url + api + notifications
  },

  // Api router countries
  countries: {
    index: serve.url + api + countries,
    show: serve.url + api + countries
  },

  // Api goals
  goals: {
    index: serve.url + api + goals,
    show: serve.url + api + goals,
    store: serve.url + api + goals,
    update: serve.url + api + goals,
    destroy: serve.url + api + goals
  }
};
