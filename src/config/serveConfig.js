const { config } = require('./config')

/*
* CONFIG SERVE
* system configuration file.
* author: Gian Vespa
*/

// config port app front
var port = config.APP_ENV || 8080;

// environment variable check
if (config.APP_ENV === 'production') {
    var url = config.URL_BACK_PRODUCTION
} else {
    var url = config.URL_BACK_LOCAL
}

module.exports.serve = {
    port: port,
    url: url
}