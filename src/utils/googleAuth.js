import { Notify } from "quasar";
import { LocalStorage } from "quasar";
import auth from "../helper/auth"

export const googleRegister = async (res) => {
    let userData = {
        fullname: res.user.displayName,
        email: res.user.email,
        image: res.user.photoURL,
    };

    const notif = Notify.create({
        type: "ongoing",
        message: "Por favor espere...",
        position: "top",
    });
    try {
        let res = await auth.googleOauth(userData);
        auth.setUserLogged(res.data.token);

        let user = await auth.show();

        let userInfo = {
            id: user.data.id,
            fullname: user.data.fullname,
            email: user.data.email,
            img: user.data.img,
            tutorial: user.data.confirm,
            country: user.data.country,
            country_id: user.data.country_id,
            password: user.data.password,
        };

        LocalStorage.set("userInfo", userInfo);
        notif();

        return userInfo
    } catch (err) {
        console.log(err);
        notif({
            type: "negative",
            message: "Ocurrió un error",
        });
    }
}