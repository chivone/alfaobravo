export default {
    splitFirstname(fullname) {
        let firstnameUnsplit = fullname.split(" ")[0];
        let firstnameSplit = firstnameUnsplit.split("_");
        let firstname = "";

        if (firstnameSplit.length > 1) {
            firstnameSplit.forEach(name => firstname += `${name} `);
        } else {
            firstname = firstnameUnsplit;
        }

        return firstname;
    },

    splitLastname(fullname) {
        let validateEmptyLast = fullname.split(" ");

        if (validateEmptyLast.length <= 1) return

        let lastnameUnsplit = fullname.split(" ")[1];
        let lastnameSplit = lastnameUnsplit.split("_");
        let lastname = "";

        if (lastnameSplit.length > 1) {
            lastnameSplit.forEach(name => lastname += `${name} `);
        } else {
            lastname = lastnameUnsplit;
        }

        return lastname
    },

    formatFirstname(firstname) {
        let firstnameSplit = firstname.split(" ");
        let first = "";

        if (firstnameSplit.length > 1) {
            firstnameSplit.forEach(name => first += `${name}_`);
        } else {
            first = firstname;
        }

        return first;
    },

    formatLastname(lastname) {
        if (lastname == "") return

        let lastnameSplit = lastname.split(" ");
        let last = "";

        if (lastnameSplit.length > 1) {
            lastnameSplit.forEach(name => last += `${name}_`);
        } else {
            last = lastname;
        }

        return last;
    }
}